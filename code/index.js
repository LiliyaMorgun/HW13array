const styles = ["«Джаз»", "«Блюз»"];
document.write("<p>Исходный массив: " + styles.join(", "));
document.write("<hr>")

styles.push("«Рок-н-ролл»");
document.write("<p>Массив после добавления: " + styles.join(", "));
document.write("<hr>")


const stylesChange = Math.floor(styles.length / 2);
styles.splice(stylesChange, 1, "«Классика»");
document.write("<p>Массив после замены: " + styles.join(", "));
document.write("<hr>")

const styles_new = styles.shift(0);
document.write("<p>Новый массив, включающий 1-й элемент исходного: " + styles_new);
document.write("<hr>")

{const styles = ["«Джаз»", "«Блюз»"];
styles.unshift("«Рэп»", "«Регги»")
document.write("<p>Массив после добавления 2-х элементов: " + styles.join(", "));
document.write("<hr>")}